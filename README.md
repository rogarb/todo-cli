# This project has moved to https://framagit.org/rogarb/todo-cli

`todo-cli`: a command line tool to manage a todofile

This program is designed to manage a file used by `todolist_bg` (see 
[https://www.gitlab.com/rogarb/todolist_bg]

Commands
---
```
 - add [-n num] [text]	    add a new item containing [text] at line [num]. 
			    Fires $EDITOR if [text] is not provided. Adds a new
			    item at the end of the list if -n option is not 
			    provided
 - rm <numlist> | <range>   removes the entry at specified lines. Arguments
			    can be a space separated list of line numbers
                            or a line range: rm 1-5 will delete line 1 to 5
                            whereas rm 8-3 will delete everything except
                            lines 4 to 7
 - mv <oldline> <newline>   moves item from <oldline> to <newline>
 - edit [-n num]	    edit the todofile. Fires $EDITOR with the full file
			    or the specified line if -n is provided.
 - show			    show the line numbered content of the todofile
 - ls			    alias for show
```

File format
---
The todo file contains one todo item per line
Item ordering is the same as line ordering

Design
---
`todo-cli` relies on standard command-line tools, like `sed`, `cat` and `awk`.
Arguments are checked by the program for integrity and the proper tool carries
out the requested action through an exec() call.
`todo-cli` supports todofile versioning with git (depends on libgit2, package
libgit2-dev on Debian). To activate it, add -DWITH_GIT to CFLAGS in the 
Makefile (activated by default).
