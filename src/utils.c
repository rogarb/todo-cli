#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <limits.h>
#include <stdarg.h>

#include "configfile.h"
#include "macro.h"
#include "utils.h"

/* return the number of lines in the todofile */
/* optimized version: we use the POSIX interface (not stdlib) and read blocks
 * instead of single chars */
int get_nlines(void)
{
    int nlines = 0;
    int fd;
    const int len = 1024;
    char *buf = NULL;
    int nread = 0;

    assert_todofile();

    str_malloc(buf, len); /* buff is initialized at len +1 */

    if ((fd = open(conf->todofile, O_RDONLY)) < 0)
	error("Unable to open %s in readonly mode: %s",
		conf->todofile, strerror(errno));

    /* optimized implementation: read a string buffer and look for newlines */
    while ((nread = read(fd, buf, len)) > 0) {
	char *p = buf;
	p[nread] = '\0';
	while ((p = strchr(p, '\n')) != NULL) {
	    p++;
	    nlines++;
	}
    }
    if (nread == -1)
	error("unable to read %s: %s", conf->todofile, strerror(errno));
    close(fd);
    free(buf);
    dbg("Calculated nlines = %d", nlines);
    return nlines;
}

/* return the number of lines in the todofile archive file */
int get_nlines_backup(void)
{
    int nlines = 0;
    int fd;
    const int len = 1024;
    char *buf = NULL;
    int nread = 0;

    assert_todofile();

    str_malloc(buf, len); /* buff is initialized at len +1 */

    if ((fd = open(conf->archive, O_RDONLY)) < 0)
	error("Unable to open %s in readonly mode: %s",
		conf->archive, strerror(errno));

    /* optimized implementation: read a string buffer and look for newlines */
    while ((nread = read(fd, buf, len)) > 0) {
	char *p = buf;
	p[nread] = '\0';
	if (*p == '=' && *(p+1) == '=')
	    nlines--; /* remove the counted archive tag line */
	while ((p = strchr(p, '\n')) != NULL) {
	    p++;
	    dbg("$p = %c, *(p+1) = %c", *p, *(p+1));
	    if (*p == '=' && *(p+1) == '=')
		nlines--; /* remove the counted archive tag line */
	    nlines++;
	}
    }
    if (nread == -1)
	error("unable to read %s: %s", conf->todofile, strerror(errno));
    close(fd);
    free(buf);
    dbg("Calculated nlines = %d", nlines);
    return nlines;
}

/* returns 1 if string contains only digits (0 to 9), 0 elsewise */
int is_number(char *string)
{
    char *p = string;
    int ret = 1;
    char c;

    while ((c = *p++)) {
	if (!isdigit(c)) {
	    ret = 0;
	    break;
	}
    }
    dbg("\"%s\" is%sa number", string, (ret ? " " : " not "));
    return ret;
}

/* return 1 if linenum is a valid line number, 0 elsewise */
int is_valid_line_number(int linenum)
{
    int ret = 1;
    if (linenum < 1 || linenum > get_nlines())
	ret = 0;
    dbg("Line number is%svalid", (ret ? " " : " not "));
    return ret;
}

/* returns 1 if string is a valid range (see get_range() below), 0 elsewise */
int is_valid_range(char *string)
{
    int ret = 1;
    char *first_pos = strchr(string, '-');
    char *last_pos = strrchr(string, '-');

    if (first_pos == NULL || first_pos == string || first_pos != last_pos)
	ret = 0;

    dbg("\"%s\" is%sa valid range", string, (ret ? " " : " not "));
    return ret;
}

/* stores the first and last elements defined in range in first and last
 * range is assumed to be XX-YYY, with XX and YY ascii representations of
 * numbers. NO CHECKS ARE MADE IN THE FUNCTION TO ENSURE THE VALIDITY OF 
 * THE INPUT, see is_valid_range() */
void get_range(char *range, int *first, int *last)
{
    char *pos = strchr(range, '-');
    if (pos != NULL) {
	*first = atoi(range);
	*last = atoi(pos+1);
    } else {
	error("BUG: range is not valid");
    }
    dbg("found first = %d, last = %d in range \"%s\"", *first, *last, range);
}

/* checks the existence of item in list */
int has_item(int item, int *list, int count)
{
    int ret = 0;

    for (int i = 0 ; i < count ; i++) {
	if (list[i] == item) {
	    ret = 1;
	    break;
	}
    }
    return ret;
}

/* check that the file exists, we expect a full path !!
 * return 1 if file exists, 0 elsewise */
int file_exists(char *filename)
{
    struct stat unused;

    if (stat(filename, &unused) < 0)
	return 0;

    return 1;
}

/* returns the number of digits of the argument in base 10 representation
 * it was previously implemented as a macro using log10 but the "bruteforce"
 * version is a lot more efficient and removes the dependency on math lib */
int intlen(int i)
{
    int n = 0;	    /* number of digits */
    /* account for one sign if i < 0 */
    int neg = (i < 0 ? 1 : 0);

    /* take the absolute value */
    if (i < 0)
	i = (i == INT_MIN ? INT_MAX : -i); /* because -INT_MIN overflows */

    if (i < 10)
	n = 1;
    else if (i < 100)
	n = 2;
    else if (i < 1000)
	n = 3;
    else if (i < 10000)
	n = 4;
    else if (i < 100000)
	n = 5;
    else if (i < 1000000)
	n = 6;
    else if (i < 10000000)
	n = 7;
    else if (i < 100000000)
	n = 8;
    else if (i < 1000000000)
	n = 9;
#if INT_MAX == 9223372036854775807  /* int is 64 bits */
    else if (i < 10000000000)
	n = 10;
    else if (i < 100000000000)
	n = 11;
    else if (i < 1000000000000)
	n = 12;
    else if (i < 10000000000000)
	n = 13;
    else if (i < 100000000000000)
	n = 14;
    else if (i < 1000000000000000)
	n = 15;
    else if (i < 10000000000000000)
	n = 16;
    else if (i < 100000000000000000)
	n = 17;
    else if (i < 1000000000000000000)
	n = 18;
    else if (i < 10000000000000000000)
	n = 19;	    /* 2^63 - 1 hits here, for 64 bit ints 
		     * (works also with 32 bit ints */
    else
	n = 20;
#else
    else
	n = 10;
#endif	/* INT_MAX == 9223372036854775807 */


    return n + neg;
}

/* execute a command built with a format string */
void exec_cmd(const char *fmt, ...)
{
    char *cmd = NULL;
    char *p = (char *) fmt; /* fix compiler warning */
    int len = strlen(fmt);
    int s_count = 0;
    int d_count = 0;
    va_list args;

    va_start(args, fmt);

    while (*p) {
	if (*p++ == '%') {
	    switch (*p) {
		case '%':
		    p++;
		    break;
		case 's':
		    len += strlen(va_arg(args, char *));
		    s_count++;
		    break;
		case 'd':
		    len += intlen(va_arg(args, int));
		    d_count++;
		    break;
		default:
		    error("BUG: unexpected identifier %c", *p);
		    break;
	    }
	}
    }

    va_end(args);

    /* allocate memory for cmd */
    if ((cmd = malloc((len+1)*sizeof(char))) == NULL)
	error("unable to allocate %d bytes of memory: %s",
	    len+1, strerror(errno));

    memset(cmd, 0, len+1);
    /* use vsnprintf to fill cmd */
    va_start(args, fmt);
    vsnprintf(cmd, len, fmt, args);
    va_end(args);

    dbg("fmt = \"%s\", got command \"%s\"", fmt, cmd);
    if (system(cmd) == -1)
	error("unable to execute command \"%s\": %s", cmd, strerror(errno));
    free(cmd);
}

/* check if file is empty: returns 1 for an empty file, 0 elsewise */
int file_is_empty(char *filename)
{
    struct stat s;
    int ret = 0;

    if (stat(filename, &s) == -1) {
	printf("Error: unable to get stats for file %s: %s\n",
		filename, strerror(errno));
	return 0;
    }

    if (s.st_size == 0)
	ret = 1;

    dbg("%s is%sempty (st_size = %ld)", filename, (ret ? " " : " not "), s.st_size);
    return ret;
}

/* IN PLACE remove beginning and ending spaces and newlines in string
 * /!\ string has to be writable (not a pointer to const line string = "bla") */
void strip_spaces(char *string)
{
    dbg("received \"%s\"", string);

    char *s;
    char *p;

    /* strip starting spaces */
    while ( *string == ' ' ) {
	s = string;
	p = string+1;
	while (*p)
	   *s++ = *p++;
    }
    
    s = string + strlen(string) - 1; /* last non NULL char of string */

    /* strip ending spaces and newlines */
    while (*s == ' ' || *s == '\n') {
	*s = '\0';
	if (s-- == string)
	    break;
    }
    dbg("sent \"%s\"", string);
}

/* expands a path containing shell globbing characters
 * takes a malloc()'ed string and expands it using realloc()
 * currently suported characters: ~
 */
char *expand(char *path)
{
    if (path == NULL)
	return NULL;
    else if (*path != '~')
	return path;

    /* ignore starting '~' char */
    int ini_len = strlen(path+1);
    char *home_env = "HOME";
    char *home = getenv(home_env);
    int len = ini_len;

    if (home == NULL)
	error("environment variable %s is not set", home_env);

    len += strlen(home);
    if ((path = realloc(path, (len)*sizeof(char))) == NULL)
	error("unable to reallocate memory");

    /* rework path to insert home at the beginning */
    memmove(path+strlen(home), path+1, ini_len);
    memcpy(path, home, strlen(home));
    /* make sure the string is '\0' terminated */
    path[len] = '\0';
    return path;
}

/* returns a malloc()'ed string containing the basedir of the given path */
char *get_basedir(char *path)
{
    char *last_slash = strrchr(path, '/');
    char *basedir = NULL;
    int len;

    if (last_slash != NULL) {
	len = last_slash - path;
	str_malloc(basedir, len);
	memcpy(basedir, path, len);
    }

    return basedir;
}

/* returns a malloc()'ed string containing the basename of the given path */
char *get_basename(char *path)
{
    char *last_slash = strrchr(path, '/');
    char *basename = NULL;
    int len;

    if (last_slash != NULL) {
	len = strlen(last_slash+1);
	str_malloc(basename, len);
	memcpy(basename, last_slash+1, len);
    }

    return basename;
}

/* return a malloc()'ed string containing the printed string
 * uses format string as printf (for now only %d and %s are supported) */
char *build_str(char *fmt, ...)
{
    char *str = NULL;
    char *p = fmt; /* fix compiler warning */
    int len = strlen(fmt);
    int s_count = 0;
    int d_count = 0;
    va_list args;

    va_start(args, fmt);

    while (*p) {
	if (*p++ == '%') {
	    switch (*p) {
		case '%':
		    p++;
		    break;
		case 's':
		    len += strlen(va_arg(args, char *));
		    s_count++;
		    break;
		case 'd':
		    len += intlen(va_arg(args, int));
		    d_count++;
		    break;
		default:
		    error("BUG: unexpected identifier %c", *p);
		    break;
	    }
	}
    }

    va_end(args);

    /* allocate memory for cmd */
    str_malloc(str, len);

    /* use vsnprintf to fill cmd */
    va_start(args, fmt);
    vsnprintf(str, len, fmt, args);
    va_end(args);

    /* shrink string to actually used memory */
    len = strlen(str);
    if ((str = realloc(str, len+1)) == NULL)
	error("unable to reallocate memory: %s", strerror(errno));
    return str;
}

/* check if fullpath is a directory : returns 1 if yes, 0 elsewise */
int path_is_dir(char *fullpath)
{
    struct stat s;
    int ret = 0;

    if (stat(fullpath, &s) == -1) {
	printf("Error: unable to get stats for file %s: %s\n",
		fullpath, strerror(errno));
	return 0;
    }

    if ((s.st_mode & S_IFMT) == S_IFDIR)
	ret = 1;
    return ret;
}

/* ask user confirmation: returns 1 if user agrees, 0 elsewise */
int ask_user_confirmation(char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);

    printf("Enter 'y' to continue: ");
    fflush(stdout);
    int c = getchar();
    if (c != 'y') {
	printf("Aborting.\n");
	return 0;
    }
    else {
	printf("Proceeding...\n");
	return 1;
    }
}

/* create a file at fullpath containing content
 * /!\ DOESN'T CHECK IF FILE EXISTS, OVERWRITE RISK /!\ */
void create_file(char *fullpath, char *content)
{
    /* Make sure we don't trigger a segfault */
    if (fullpath == NULL)
	error("BUG: %s called with a NULL pathname", __func__);
    FILE *f = NULL;
    if ((f = fopen(fullpath, "w")) == NULL)
	error("error opening %s in write mode: %s", fullpath, strerror(errno));
    /* set content to empty string if NULL */
    if (content == NULL)
	content = "";
    fprintf(f, "%s", content);
    dbg("created %s", fullpath);
    /* cleanup */
    fclose(f);
}

/* append content at the end of file designated by fullpath */
void append_file(char *fullpath, char *content)
{
    /* Make sure we don't trigger a segfault */
    if (fullpath == NULL)
	error("BUG: %s called with a NULL pathname", __func__);
    /* silently ignore NULL content */
    if (content == NULL)
	return;

    FILE *f = NULL;
    if ((f = fopen(fullpath, "a")) == NULL)
	error("error opening %s in append mode: %s", fullpath, strerror(errno));
    
    fprintf(f, "%s", content);
    
    /* cleanup */
    fclose(f);
}

/* returns 1 if buffer contains only whitespaces, 0 otherwise */
int is_empty_line(char *line)
{
    if (line == NULL)
	error("BUG: provided with a NULL pointer");

    char c;
    while ((c = *line++)) {
	if (!isspace(c))
	    return 0;
    }
    return 1;
}
