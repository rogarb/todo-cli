#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>

#include "command.h"
#include "utils.h"
#include "macro.h"
#include "processing.h"
#include "configfile.h"

#ifdef WITH_GIT
#include "git.h"
#endif /* WITH_GIT */

char *progname = NULL;

/* private functions */
#ifdef DEBUG
static char *code2str(int cmd_code);
#endif
static void add_command(int argc, char *argv[]);
static void rm_command(int argc, char *argv[]);
static void mv_command(int argc, char *argv[]);
static void swap_command(int argc, char *argv[]);
static void edit_command(int argc, char *argv[]);
static void show_command(int argc, char *argv[]);
static void archive_command(int argc, char *argv[]);
static void lsarchive_command(int argc, char *argv[]);
static void restore_command(int argc, char *argv[]);
#ifdef WITH_GIT
static void undo_command(int argc, char *argv[]);
#endif /* WITH_GIT */
#define help_command(a, b) usage()  /*  defined for naming consistency */

void usage(void)
{
    if (progname == NULL) 
	error("BUG: progname is not set");

    printf("Usage: %s command [args]\n", progname);
    printf(
" CLI todofile manager. The path to the todofile can be ser either through a\n"
" configfile located at %s or the environment variable %s\n\n", 
	    get_configfile(),TODOFILE_ENV);
    printf(
"Available commands:\n"
" - add [-n num] [text]       add a new item containing [text] at line [num].\n"
"                             Fires $EDITOR if [text] is not provided. Adds a\n"
"                             new item at the end of the list if -n option is\n"
"                             not provided\n"
" - rm <numlist> | <range>    remove the entry at specified lines. Arguments\n"
"                             can be a space separated list of line numbers\n"
"                             or a line range: rm 1-5 will delete line 1 to 5\n"
"                             whereas rm 8-3 will delete everything except\n"
"                             lines 4 to 7\n"
" - mv <oldline> <newline>    move the item from <oldline> to <newline>\n"
" - swap <line1> <line2>      swap <line1> and <line2>\n"
" - edit [-n num]             edit the todofile. Fires $EDITOR loaded with the\n"
"                             full file or the specified line if -n is provided.\n"
" - show                      show the line numbered content of the todofile\n"
" - ls                        alias for the show command\n"
" - archive [numlist|range]   archive the content of the todofile\n"
" - ls-archive                list the content of the archive file\n"
" - restore [numlist|range]   restore the content to the todofile\n"
#ifdef WITH_GIT
" - undo                      undo the last action on the todofile\n"
#endif
	);
    exit(1);
}

void launch(int cmd_code, int argc, char *argv[])
{
    dbg("Launching command %s", code2str(cmd_code));

    switch (cmd_code) {
	case CMD_ADD:
	    add_command(argc, argv);
	    break;
	case CMD_RM:
	    rm_command(argc, argv);
	    break;
	case CMD_MV:
	    mv_command(argc, argv);
	    break;
	case CMD_SWAP:
	    swap_command(argc, argv);
	    break;
	case CMD_EDIT:
	    edit_command(argc, argv);
	    break;
	case CMD_SHOW:
	    show_command(argc, argv);
	    break;
	case CMD_AR:
	    archive_command(argc, argv);
	    break;
	case CMD_LSAR:
	    lsarchive_command(argc, argv);
	    break;
	case CMD_RESTORE:
	    restore_command(argc, argv);
	    break;
#ifdef WITH_GIT
	case CMD_UNDO:
	    undo_command(argc, argv);
	    break;
#endif
	default:
	    printf("Error: no command named %s\n", argv[0]);
	case CMD_HELP:
	    help_command(argc, argv);
	    break;
    }
}

#ifdef WITH_GIT
static void undo_command(int argc, char *argv[])
{
    revert_last_commit();
}
#endif

static void restore_command(int argc, char *argv[])
{
    int nlines;	    /* number of lines in the todofile */
    int line;
    char *sline;

    assert_todofile();
    nlines = get_nlines_backup();

    if (argc == 1) { /* restore everything */
	/* ask user confirmation */
	if (ask_user_confirmation("This will restore all items in the list.\n"))
	    restore_range(1, nlines);
    } else if (argc == 2 && !is_number(argv[1])) { /* this is a range */
	if (!is_valid_range(argv[1]))
	    error("%s is not a valid range. See help", argv[1]);

	int first, last;
	get_range(argv[1], &first, &last);
	if (first > nlines || last > nlines)
	    error("Invalid range: line number should be within range 1-%d",
		    nlines);

	/* check for ranges which resolve to the entire file and ask user
	 * confirmation */
	if (first == last+1 || (first == 1 && last == nlines)) {
	    printf("Using range %d-%d.\n", first, last);
	    if (ask_user_confirmation(
		    "This will restore all items in the list.\n") == 0)
		return;
	}
	restore_range(first, last);
    } else {
	/* build an array of unique line numbers */
	int len = argc - 1;
	int *items = malloc(len*sizeof(int));
	if (items == NULL)
	    error("unable to allocate memory: %s", strerror(errno));

	dbg("created array of %d elements", len);
	memset(items, 0, len*sizeof(*items));
	int uniq = 0;
	for (int i = 1 ; i < argc ; i++) {
	    sline = argv[i];
	    line = atoi(sline);
	    /* check that the argument is valid */
	    if (line > nlines)
		error("Line number must be within the line range (1-%d)",
			nlines);

	    if (line <= 0)
		error("Line number must be at least 1");

	    if (has_item(line, items, len))
		warn("Repeated line number %d", line);
	    else
		items[uniq++] = line;
	}
#ifdef DEBUG
	dbg("Attempting to restore items :");
	for (int i = 0 ; i < uniq ; i++)
	    dbg("items[%d] = %d", i, items[i]);
#endif
	restore_items(items, uniq);
	free(items);
#ifdef WITH_GIT
    if (conf->git)
	commit_todofile("Restored content");
#endif /* WITH_GIT */
    }
}

static void archive_command(int argc, char *argv[])
{
    int nlines;	    /* number of lines in the todofile */
    int line;
    char *sline;

    assert_todofile();
    nlines = get_nlines();

    if (argc == 1) { /* archive everythin */
	/* ask user confirmation */
	if (ask_user_confirmation("This will archive all items in the list.\n"))
	    archive_range(1, nlines);
	else 
	    return;
    } else if (argc == 2 && !is_number(argv[1])) { /* this is a range */
	if (!is_valid_range(argv[1]))
	    error("%s is not a valid range. See help", argv[1]);

	int first, last;
	get_range(argv[1], &first, &last);
	if (first > nlines || last > nlines)
	    error("Invalid range: line number should be within range 1-%d",
		    nlines);

	/* check for ranges which resolve to the entire file and ask user
	 * confirmation */
	if (first == last+1 || (first == 1 && last == nlines)) {
	    printf("Using range %d-%d.\n", first, last);
	    if (ask_user_confirmation(
		    "This will archive all items in the list.\n") == 0)
		return;
	}
	archive_range(first, last);
    } else {
	/* build an array of unique line numbers */
	int len = argc - 1;
	int *items = malloc(len*sizeof(int));
	if (items == NULL)
	    error("unable to allocate memory: %s", strerror(errno));

	dbg("created array of %d elements", len);
	memset(items, 0, len*sizeof(*items));
	int uniq = 0;
	for (int i = 1 ; i < argc ; i++) {
	    sline = argv[i];
	    line = atoi(sline);
	    /* check that the argument is valid */
	    if (line > nlines)
		error("Line number must be within the line range (1-%d)",
			nlines);

	    if (line <= 0)
		error("Line number must be at least 1");

	    if (has_item(line, items, len))
		warn("Repeated line number %d", line);
	    else
		items[uniq++] = line;
	}
#ifdef DEBUG
	dbg("Attempting to archive items :");
	for (int i = 0 ; i < uniq ; i++)
	    dbg("items[%d] = %d", i, items[i]);
#endif
	archive_items(items, uniq);
	free(items);
#ifdef WITH_GIT
    if (conf->git)
	commit_todofile("Archived content");
#endif /* WITH_GIT */
    }
}

static void lsarchive_command(int argc, char *argv[])
{
    assert_todofile();
    
    if (argc != 1) {
	printf("%s command doesn't take an argument\n", argv[0]);
	usage();
    }

    if (!file_exists(conf->archive)) {
	printf("No backup file to list (%s)\n", conf->archive);
	return;
    }
    /* we use cat -n to number the lines */
    exec_cmd("grep -v -e '^==' %s | cat -n", conf->archive); 
}

static void show_command(int argc, char *argv[])
{
    assert_todofile();

    if (argc != 1) {
	printf("%s command doesn't take an argument\n", argv[0]);
	usage();
    }
    /* we use cat -n to number the lines */
    char *command = "cat";
    char *opt = "-n";
    char *cmd_table[] = { command, opt, conf->todofile, NULL };

    execvp(command, cmd_table);
    /* reached only if execvp cannot start */
    error("unable to execute command %s: %s", command, strerror(errno));
}

static void add_command(int argc, char *argv[])
{
    int opt;
    int nlines = get_nlines();
    int line = nlines +1 ;  /* line number arg, defaults to lastline + 1 */
    char *sline = NULL; /* string version of the line number arg */
    char *tmpfile;

    assert_todofile();

    while ((opt = getopt(argc, argv, "+n:")) != -1) {
	switch (opt) {
	    case 'n':
		sline = optarg;
		break;
	    default:
		usage();
		break;
	}
    }
    if (sline != NULL) {
	line = atoi(sline);
	if (line <= 0)
	    error("Invalid line number %d (%s)", line, sline);
	if (line > nlines+1) {
	    warn("line number above range, appending at the end of file");
	    line = nlines + 1;
	}
    }

    /* don't create the tmpfile before as we might exit */
    tmpfile = mk_tmpfile();

    if (optind < argc) { /* treat all remaining args as text to append */
	cat2file(tmpfile, argc-optind, argv+optind);
    } else {
	/* start editor on tmpfile */
	start_editor(tmpfile);
    }

    remove_empty_lines(tmpfile);
    add_item(tmpfile, line);
    cleanup(tmpfile);
#ifdef WITH_GIT
    if (conf->git)
	commit_todofile("Added content");
#endif /* WITH_GIT */
}

static void mv_command(int argc, char *argv[])
{
    int from = 0;
    int to = 0;
    int *args[] = { &from, &to };
    char *tmpfile = NULL;

    if (argc != 3)
	error("Bad number of arguments, see help");

    argv++; /* so we can use the same index for argv and args */
    for (int i = 0 ; i < 2 ; i++) {
	if (!is_number(argv[i]))
	    error("bad argument: %s is not a number", argv[i]);
	else
	   *args[i] = atoi(argv[i]);
	if (!is_valid_line_number(*args[i]))
	    error("bad argument: %d is above last line", *args[i]);
	if (from == to)
	    error("not moving a line to itself");
    }
    tmpfile = mk_tmpfile();
    extract_item(tmpfile, from);
    remove_item(from);
    add_item(tmpfile, to);
    cleanup(tmpfile);
#ifdef WITH_GIT
    if (conf->git)
	commit_todofile("Moved content");
#endif /* WITH_GIT */
}

static void swap_command(int argc, char *argv[])
{
    int items[2] = { 0, 0};
    char *tmpfile = NULL;

    if (argc != 3)
	error("Bad number of arguments, see help");

    argv++; /* so we can use the same index for argv and items */
    for (int i = 0 ; i < 2 ; i++) {
	if (!is_number(argv[i]))
	    error("bad argument: %s is not a number", argv[i]);
	else
	   items[i] = atoi(argv[i]);
	if (!is_valid_line_number(items[i]))
	    error("bad argument: %d is above last line", items[i]);
	if (items[0] == items[1])
	    error("not moving a line to itself");
    }
    /* order the args */
    if (items[0] > items[1]) {
	int tmp = items[0];
	items[0] = items[1];
	items[1] = tmp;
    }
    /* decrement the maximum item */
    items[1] = (--items[1] == 0 ? 1 : items[1]);
    tmpfile = mk_tmpfile();
    /*  extract and remove the first item */
    extract_item(tmpfile, items[0]);
    remove_item(items[0]);
    /* add it back at the right position, which we increment */
    add_item(tmpfile, items[1]++);
    /* extract and remove the second item, which is at its original position */
    extract_item(tmpfile, items[1]);
    remove_item(items[1]);
    /* add it back at the right position */
    add_item(tmpfile, items[0]);
    cleanup(tmpfile);
#ifdef WITH_GIT
    if (conf->git)
	commit_todofile("Swapped content");
#endif /* WITH_GIT */
}

static void rm_command(int argc, char *argv[])
{
    int nlines;	    /* number of lines in the todofile */
    int line;
    char *sline;

    assert_todofile();
    nlines = get_nlines();

    if (argc < 2)
	error("Command %s takes at least one argument", argv[0]);

    if (argc == 2 && !is_number(argv[1])) { /* this is a range */
	if (!is_valid_range(argv[1]))
	    error("%s is not a valid range. See help", argv[1]);

	int first, last;
	get_range(argv[1], &first, &last);
	if (first > nlines || last > nlines)
	    error("Invalid range: line number should be within range 1-%d",
		    nlines);

	/* check for ranges which resolve to the entire file and ask user
	 * confirmation */
	if (first == last+1 || (first == 1 && last == nlines)) {
	    printf("Using range %d-%d.\n", first, last);
	    printf("This will remove all items in the list.\n");
	    printf("Enter 'y' to continue: ");
	    int c = getchar();
	    if (c != 'y') {
		printf("Aborting.\n");
		return;
	    }
	    else
		printf("Deleting everything...\n");
	}
	remove_range(first, last);
    } else {
	/* build an array of unique line numbers */
	int len = argc - 1;
	int *items = malloc(len*sizeof(int));
	if (items == NULL)
	    error("unable to allocate memory: %s", strerror(errno));

	dbg("created array of %d elements", len);
	memset(items, 0, len*sizeof(*items));
	int uniq = 0;
	for (int i = 1 ; i < argc ; i++) {
	    sline = argv[i];
	    line = atoi(sline);
	    /* check that the argument is valid */
	    if (line > nlines)
		error("Line number must be within the line range (1-%d)",
			nlines);

	    if (line <= 0)
		error("Line number must be at least 1");

	    if (has_item(line, items, len))
		warn("Repeated line number %d", line);
	    else
		items[uniq++] = line;
	}
#ifdef DEBUG
	dbg("Attempting to remove items :");
	for (int i = 0 ; i < uniq ; i++)
	    dbg("items[%d] = %d", i, items[i]);
#endif
	remove_items(items, uniq);
	free(items);
#ifdef WITH_GIT
    if (conf->git)
	commit_todofile("Removed content");
#endif /* WITH_GIT */
    }
}

static void edit_command(int argc, char *argv[])
{
    assert_todofile();

    /* no arguments to parse, we edit the todofile directly */
    if (argc == 1) {
	start_editor(conf->todofile);
	remove_empty_lines(conf->todofile);
#ifdef WITH_GIT
	if (conf->git)
	    commit_todofile("Edited full file");
#endif /* WITH_GIT */
	return;
    }

    /* arguments to parse */
    int line = 0;	/* line number arg */
    char *sline = NULL; /* string version of the line number arg */
    char *tmpfile;
    int opt;
    int nlines = get_nlines();

    /* parse args */
    while ((opt = getopt(argc, argv, "n:")) != -1) {
	switch (opt) {
	    case 'n':
		sline = optarg;
		break;
	    default:
		usage();
		break;
	}
    }

    if (sline != NULL)
	line = atoi(sline);

    if (line <= 0 || line > nlines)
	error("Line number %d out of range [1 - %d]", line, nlines);

    tmpfile = mk_tmpfile();
    extract_item(tmpfile, line);

    /* edit the tmpfile with $EDITOR */
    start_editor(tmpfile);
    remove_empty_lines(tmpfile);
    replace_item(tmpfile, line);
    cleanup(tmpfile);
#ifdef WITH_GIT
    if (conf->git)
	commit_todofile("Edited line");
#endif /* WITH_GIT */
}

#ifdef DEBUG
static char *code2str(int cmd_code)
{
    char *ret = NULL;
    switch (cmd_code) {
	case CMD_HELP:
	    ret = "help";
	    break;
	case CMD_ADD:
	    ret = "add";
	    break;
	case CMD_RM:
	    ret = "rm";
	    break;
	case CMD_EDIT:
	    ret = "edit";
	    break;
	case CMD_SHOW:
	    ret = "show/ls";
	    break;
	default:
	    ret = "error";
	    break;
    }
    return ret;
}
#endif
