#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <time.h>

#include "processing.h"
#include "macro.h"
#include "utils.h"
#include "configfile.h"

void start_editor(char *tmpfile)
{
    char *env = "EDITOR";
    char *cmd = getenv(env);

    if (tmpfile == NULL)
	error("[BUG] tempfile not created");

    if (cmd == NULL)
	error("unable to get environment variable %s: %s",
		env, strerror(errno));
    exec_cmd("%s %s", cmd, tmpfile);
}

/* merge tmpfile into todo file at specified position */
/* uses cat or sed depending on the case */
void add_item(char *tmpfile, int linenum)
{
    dbg("using tmpfile = %s, linenum = %d", tmpfile, linenum);

    if (file_is_empty(tmpfile))
	return;

    int nlines = get_nlines();

    if (linenum < 0)
	error("linenumber cannot be negative");

    /* if linenum is not set, set it to enter append mode */
    if (linenum == 0)
	linenum = nlines+1;

    if (linenum > nlines) {
	if (linenum > nlines+1)
	    warn(
	    "Requested linenumber (%d) above range (%d). Appending at line %d",
		linenum, nlines+1, nlines+1);
	dbg("Executing command");
	exec_cmd("cat %s >> %s", tmpfile, conf->todofile);
    } else {
	/* use sed to insert the line. /!\ we use a GNU extension ! */
	dbg("Executing command");
	exec_cmd("sed -i -e '%de cat %s' %s", linenum, tmpfile, conf->todofile);
    }
}

/* we assume that tmpfile has been created with the right permission
 * and that linenum has a proper value */
void extract_item(char *tmpfile, int linenum)
{
    dbg("executing command");
    exec_cmd("awk 'NR==%d {print}' %s >> %s", linenum, conf->todofile, tmpfile);
}

/* extract item from backup file, see comment at extract_item()  */
void extract_item_backup(char *tmpfile, int linenum)
{
    if (file_exists(conf->archive) == 0) {
	error("Archive file (%s) doesn't exist", conf->archive);
    }
    linenum *= 2; /* the item is always precedeed by a tag line so we need to
		   * recalculate the line number */
    dbg("executing command");
    exec_cmd("awk 'NR==%d {print}' %s >> %s", linenum, conf->archive, tmpfile);
}

/* restore an item */
void restore_item(int linenum)
{
    char *tmp = NULL;
    tmp = mk_tmpfile();

    extract_item_backup(tmp, linenum);
    cleanup(tmp);
}

/* helper to archive an item */
static void __archive_item(int linenum)
{
    char *archive_tag = NULL;
    time_t now;
    if (file_exists(conf->archive) == 0) {
	/* create the non existing file without content */
	create_file(conf->archive, NULL);
    }
    now = time(NULL);
    /* ctime() returns a '\n' terminated string */
    archive_tag = build_str("== Archived on %s", ctime(&now));
    append_file(conf->archive, archive_tag);
    extract_item(conf->archive, linenum);
    free(archive_tag);
}

/* archives the linenum line of conf->todofile in $(conf->todofile).bak */
void archive_item(int linenum)
{
    __archive_item(linenum);
    remove_item(linenum);
}

/* removes the linenum line of conf->archive */
void remove_item_backup(int linenum)
{
    remove_items_backup(&linenum, 1);
}

/* removes the linenum line of conf->todofile */
void remove_item(int linenum)
{
    dbg("executing command");
    exec_cmd("sed -i -e '%dd' %s", linenum, conf->todofile);
}

/* compare ints to use in qsort */
static int compint(const void *a, const void *b)
{
    int i = *((int *) a);
    int j = *((int *) b);

    return i - j;
}

/* archive a list of items */
void restore_items(int *items, int count)
{
    if (items == NULL)
	error("BUG: called with NULL items argument");
    if (count < 0)
	error("BUG: called with negative item count");
    char *tmp = NULL;
    tmp = mk_tmpfile();
    for (int i = 0 ; i < count ; i++) {
	extract_item_backup(tmp, items[i]);
    }
    exec_cmd("cat %s >> %s", tmp, conf->todofile);
    remove_items_backup(items, count);
    cleanup(tmp);
}

/* archive a list of items */
void archive_items(int *items, int count)
{
    if (items == NULL)
	error("BUG: called with NULL items argument");
    if (count < 0)
	error("BUG: called with negative item count");
    for (int i = 0 ; i < count ; i++) {
	__archive_item(items[i]);
    }
    remove_items(items, count);
}

/* remove items and their tag */
void remove_items_backup(int *items, int count)
{
    char *cmd = NULL;
    char *base = "sed -i ";
    char *del = "-e '%dd' ";
    int offset = 0;
    int tmpcount = 2*count;
    int *tmp = malloc(2*count*sizeof(int));
    int len = strlen(base) + strlen(conf->archive) + strlen(del) * tmpcount;

    for (int i = 0 ; i < count ; i++) {
	tmp[2*i] = 2*items[i]-1;
	tmp[2*i+1] = 2*items[i];
    }
    for (int i = 0 ; i < tmpcount ; i++)
	len += intlen(tmp[i]);

    //qsort(items, count, sizeof(*items), compint);

    if ((cmd = malloc((len+1)*sizeof(char))) == NULL)
	error("unable to allocate memory: %s", strerror(errno));
    memset(cmd, 0, len+1);
    strncpy(cmd, base, len);
    offset = strlen(cmd);
    for (int i = 0 ; i < tmpcount ; i++) {
	snprintf(cmd+offset, len-offset, del, tmp[i]);
	offset = strlen(cmd);
    }
    strncpy(cmd+offset, conf->archive, len-offset);
    if (system(cmd) != 0)
	error("unable to execute \"%s\": %s", cmd, strerror(errno));
    free(cmd);
}

void remove_items(int *items, int count)
{
    char *cmd = NULL;
    char *base = "sed -i ";
    char *del = "-e '%dd' ";
    int offset = 0;
    int len = strlen(base) + strlen(conf->todofile) + strlen(del) * count;

    for (int i = 0 ; i < count ; i++)
	len += intlen(items[i]);

    qsort(items, count, sizeof(*items), compint);

    if ((cmd = malloc((len+1)*sizeof(char))) == NULL)
	error("unable to allocate memory: %s", strerror(errno));
    memset(cmd, 0, len+1);
    strncpy(cmd, base, len);
    offset = strlen(cmd);
    for (int i = 0 ; i < count ; i++) {
	snprintf(cmd+offset, len-offset, del, items[i]);
	offset = strlen(cmd);
    }
    strncpy(cmd+offset, conf->todofile, len-offset);
    if (system(cmd) == -1)
	error("unable to execute \"%s\": %s", cmd, strerror(errno));
    free(cmd);
}

void restore_range(int first, int last)
{
    char *tmp = NULL;
    tmp = mk_tmpfile();

    if (first == last) /* this is a one item range */
	extract_item_backup(tmp, first);
    else if (first < last) { /* "normal" range */
	for (int i = first ; i <= last ; i++)
	    extract_item_backup(tmp, i);
    } else {  /* "reverse" range, deletes everything excluding the interval
	       * last+1, first-1 */
	int nlines = get_nlines();
	for (int i = 1 ; i <= last ; i++)
	    extract_item_backup(tmp, i);
	for (int i = first ; i <= nlines ; i++)
	    extract_item_backup(tmp, i);
    }
    exec_cmd("cat %s >> %s", tmp, conf->todofile);
    remove_range_backup(first, last);
    cleanup(tmp);
}

/* archive a range of items */
void archive_range(int first, int last)
{
    if (first == last) /* this is a one item range */
	__archive_item(first);
    else if (first < last) { /* "normal" range */
	for (int i = first ; i <= last ; i++)
	    __archive_item(i);
    } else {  /* "reverse" range, deletes everything excluding the interval
	       * last+1, first-1 */
	int nlines = get_nlines();
	for (int i = 1 ; i <= last ; i++)
	    __archive_item(i);
	for (int i = first ; i <= nlines ; i++)
	    __archive_item(i);
    }
    remove_range(first, last);
}

void remove_range(int first, int last)
{
    if (first == last) /* this is a one item range */
	remove_item(first);
    else if (first < last) { /* "normal" range */
	exec_cmd("sed -i -e '%d,%dd' %s", first, last, conf->todofile);
    } else {  /* "reverse" range, deletes everything excluding the interval
	       * last+1, first-1 */
	int nlines = get_nlines();
	exec_cmd("sed -i -e '%d,%dd' -e '1,%dd' %s",
		first, nlines, last, conf->todofile);
    }
}

void remove_range_backup(int first, int last)
{
    if (first == last) /* this is a one item range */
	remove_items_backup(&first, 1);
    else if (first < last) { /* "normal" range */
	first = 2 * first - 1;
	last = 2 * last;
	exec_cmd("sed -i -e '%d,%dd' %s", first, last, conf->archive);
    } else {  /* "reverse" range, deletes everything excluding the interval
	       * last+1, first-1 */
	int nlines = 2*get_nlines_backup();
	first = 2 * first;
	last = 2 * last - 1;
	exec_cmd("sed -i -e '%d,%dd' -e '1,%dd' %s",
		first, nlines, last, conf->archive);
    }
}

/* replace item at line number */
void replace_item(char *tmpfile, int linenum)
{
    if (file_is_empty(tmpfile))
	return;
    remove_item(linenum);
    add_item(tmpfile, linenum);
}

/* concatenates the strings contained in array to tmpfile separating each field
 * with a space */
void cat2file(char* tmpfile, int len, char *array[])
{
    FILE *tmp;
    if ((tmp = fopen(tmpfile, "w")) == NULL)
	error("unable to open tmp file in write mode: %s", strerror(errno));

    for (int i = 0 ; i < len ; i++) {
	fputs(array[i], tmp);
	if (i == len - 1)
	    fputc('\n', tmp);
	else
	    fputc(' ', tmp);
    }
    fclose(tmp);
}

/* creates an empty tmpfile and return the string name */
char *mk_tmpfile(void)
{
    char template[] = "/tmp/todoXXXXXX";
    char *ret;
    int tmp_fd = -1;

    /* create and open a temporary file */
    if ((tmp_fd = mkstemp(template)) == -1)
	error("unable to create tmp file: %s", strerror(errno));

    close(tmp_fd);

    ret = build_str("%s", template);
#ifdef DEBUG
    file_is_empty(ret); /* this prints a debug message in file_is_empty() */
#endif
    return ret;
}

/* free the malloc()'ed file name and unlink the file if necessary */
void cleanup(char *tmpfile)
{
    /* check that tmpfile still exits */
    if (file_exists(tmpfile))
	unlink(tmpfile);
    free(tmpfile);
}

/* use sed to trim empty lines from the given file */
void remove_empty_lines(char *filename)
{
    dbg("executing command");
    exec_cmd("sed -i -r '/^\\s*$/d' %s", filename);
#ifdef DEBUG
    dbg("Printing the content of %s", filename);
    FILE *f = fopen(filename, "r");
    if (f != NULL) {
	int c;
	while ((c = fgetc(f)) != EOF)
	    putchar(c);
    } else {
	dbg("Unable to open %s in read only mode", filename);
    }
#endif
}
