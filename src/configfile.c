#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "utils.h"
#include "macro.h"
#include "configfile.h"
#include "git.h"

#define MAX_LINE_SIZE	512	    /* max chars per line in the config file */

config_t *conf = NULL;

/* private functions */
static void parse_config(char *configfile);

/* setup the configuration, exits on error */
void setup()
{
    char *configfile = get_configfile();
    char *tmp = NULL;

    if ((conf = malloc(sizeof(config_t))) == NULL)
	error("unable to allocate memory for configuration");
    memset(conf, 0, sizeof(*conf));

    if (file_exists(configfile)) {
	/* parse config */
	parse_config(configfile);
	/* free malloc()'ed string */
    } else {
	warn("no config file found");
    }
    free(configfile);

    /* overwrite the config with the environment variables */
    if ((tmp = getenv(TODOFILE_ENV)) != NULL)
	conf->todofile = tmp;

    /* check the config */
    if (conf->todofile == NULL)
	error("Todofile path must be set either with environment variable %s "
	      " or %s entry in configfile", TODOFILE_ENV, CONF_TODOFILE_KEY);

    /* set default archive field to $(todofile).bak if not set by the conf */
    if (conf->archive == NULL) {
	char *tmp = strdup(conf->todofile);
	char *suffix = ".bak";
	int newlen = strlen(tmp) + strlen(suffix);
	tmp = realloc(tmp, (newlen+1)*sizeof(char));
	if (tmp == NULL)
	    error("Unable to realloc memory for archive field");
	strncpy(tmp+strlen(conf->todofile), suffix, strlen(suffix)+1);
	conf->archive = tmp;
    }
    dbg("conf->archive set to \"%s\"", conf->archive);
#ifdef WITH_GIT
    if (conf->git)
	setup_git();
    dbg("Git %s", conf->git ? "enabled" : "disabled");
#else
    __nop_git() /* to avoid warning when WITH_GIT is not defined */
#endif /* WITH_GIT */
    dbg("using %s as todofile", conf->todofile);

}

void quit(void)
{
#ifdef WITH_GIT
    if (conf->git)
	exit_git();
#endif /* WITH_GIT */
    exit(0);
}

/* build up the configfile full path and return it as a malloc()'ed string */
char *get_configfile(void)
{
    char *cf = NULL;
    char *fmt = "%s/%s/%s"; /* $HOME/CONFIG_DIR/CONFIG_FILE */
    char *home = NULL;
    
    if ((home = getenv(HOME_ENV)) == NULL)
	error("Environment variable %s is not set.", HOME_ENV);

    cf = build_str(fmt, home, CONFIG_DIR, CONFIG_FILE);
    return cf;
}

/* parse the configuration file at configfile */
static void parse_config(char *configfile)
{
    char line_buf[MAX_LINE_SIZE+1] = { 0 };
    FILE *f = fopen(configfile, "r");
    int line_count = 0;

    if (f == NULL)
	error("unable to open file %s readonly: %s", 
		configfile, strerror(errno));

    while (fgets(line_buf, MAX_LINE_SIZE, f) != NULL) {
	if (line_buf[MAX_LINE_SIZE-1] == '\n') /* long line */
	    error("Long line detected in the config file\nAborting\n");

	line_count++;

	if (line_buf[0] == '#' || is_empty_line(line_buf))
	    continue; /* ignore commented and empty lines */

	char *comment = strchr(line_buf, '#');
	if (comment != NULL)
	    *comment = '\0'; /* strip comments */

	char *value = line_buf;
	char *key = strsep(&value, "=");

	if (value == NULL)
	    error("invalid content at line %d: %s", line_count, line_buf);

	/* remove starting and ending spaces in strings */
	strip_spaces(key);
	strip_spaces(value);
	if (strcmp(key, CONF_TODOFILE_KEY) == 0) {
	    char *tmp = strdup(value);
	    conf->todofile = expand(tmp);
	} else if (strcmp(key, CONF_ARCHIVE_KEY) == 0) {
	    char *tmp = strdup(value);
	    conf->archive = expand(tmp);
#ifdef WITH_GIT
	} else if (strcmp(key, CONF_GIT_KEY) == 0) {
	    conf->git = atoi(value);
	    if (!is_number(value) || (conf->git != 0 && conf->git != 1))
		error("invalid value %s for key %s at line %d",
		       value, CONF_GIT_KEY, line_count);
#endif /* WITH_GIT */
	} else {
	    error("invalid key %s at line %d in configfile",
		    key, line_count);
	}
    }
}
