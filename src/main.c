#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "command.h"
#include "configfile.h"

int main(int argc, char *argv[])
{
    progname = argv[0];

    /* parse command */
    if (argc < 2)
	usage();

    /* Setup */
    setup();

    char *command = argv[1];
    int cmd_code = 0;
    if (strcmp(command, "help") == 0)
	cmd_code = CMD_HELP;
    else if(strcmp(command, "add") == 0)
	cmd_code = CMD_ADD;
    else if (strcmp(command, "rm") == 0)
	cmd_code = CMD_RM;
    else if (strcmp(command, "mv") == 0)
	cmd_code = CMD_MV;
    else if (strcmp(command, "swap") == 0)
	cmd_code = CMD_SWAP;
    else if (strcmp(command, "edit") == 0)
	cmd_code = CMD_EDIT;
    else if (strcmp(command, "show") == 0)
	cmd_code = CMD_SHOW;
    else if (strcmp(command, "ls") == 0)
	cmd_code = CMD_SHOW;
    else if (strcmp(command, "archive") == 0)
	cmd_code = CMD_AR;
    else if (strcmp(command, "ls-archive") == 0)
	cmd_code = CMD_LSAR;
    else if (strcmp(command, "restore") == 0)
	cmd_code = CMD_RESTORE;
    else if (strcmp(command, "undo") == 0)
	cmd_code = CMD_UNDO;
    else
	cmd_code = CMD_ERR;

    /* launch command with the remaining of the command line as args */
    launch(cmd_code, --argc, ++argv);

    quit();
}
