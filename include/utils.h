/******************************************************************************
 * utils.h: Various utility functions
 *****************************************************************************/
#ifndef UTILS_H
#define UTILS_H
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "macro.h"

/* allocate memory in ptr which is suitable to hold a string of length len
 * memory is set to zero */
#define str_malloc(ptr, len) \
    do {\
	if ((ptr = malloc((len+1)*sizeof(char))) == NULL) \
	    error("unable to allocate %d bytes of memory: %s",\
		    len+1, strerror(errno));\
	memset(ptr, 0, (len+1)*sizeof(char));\
    } while (0)

/* strlen equivalent */
int intlen(int i);

/* file test functions */
int file_exists(char *filename);
int file_is_empty(char *filename);
int path_is_dir(char *fullpath);

/* command execution functions */
void exec_cmd(const char *fmt, ...);

/* string test functions */
int is_number(char *string);
int is_valid_range(char *string);
int is_valid_line_number(int line);
int is_empty_line(char *line);

/* information retrieving */
int get_nlines(void);
int get_nlines_backup(void);

/* string manipulation */
void get_range(char *range, int *first, int *last);
void strip_spaces(char *string);
char *expand(char *path);
char *get_basedir(char *path);
char *get_basename(char *path);
char *build_str(char *fmt, ...);

/* array related */
int has_item(int item, int *list, int count);

/* user interaction */
int ask_user_confirmation(char *fmt, ...);

/* file manipulation */
void create_file(char *fullpath, char *content);
void append_file(char *fullpath, char *content);

#endif /* End of UTILS_H */
