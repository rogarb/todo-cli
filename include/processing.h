/******************************************************************************
 * processing.h: File processing functions
 *****************************************************************************/
#ifndef PROCESSING_H
#define PROCESSING_H

void start_editor(char *filename);
void add_item(char *tmpfile, int linenum);
void extract_item(char *tmpfile, int linenum);
void replace_item(char *tmpfile, int linenum);
void remove_item(int linenum);
void remove_items(int *item, int count);
void remove_range(int first, int last);
void remove_item_backup(int linenum);
void remove_items_backup(int *item, int count);
void remove_range_backup(int first, int last);
void archive_item(int linenum);
void archive_items(int *item, int count);
void archive_range(int first, int last);
void restore_item(int linenum);
void restore_items(int *item, int count);
void restore_range(int first, int last);
void remove_empty_lines(char *filename);
void cat2file(char* tmpfile, int len, char *array[]);
char *mk_tmpfile(void);
void cleanup(char *tmpfile);

#endif /* End of PROCESSING_H */
