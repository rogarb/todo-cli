/******************************************************************************
 * macro.h: Useful macros
 *****************************************************************************/
#ifndef MACRO_H
#define MACRO_H
#include <stdio.h>
#include <stdlib.h>

/* message printing macros */
#define error(fmt, ...)  \
    do {\
	fprintf(stderr, "[ERROR] %s: " fmt "\n", __func__, ##__VA_ARGS__);\
	exit(1);\
    } while (0)

#define warn(fmt, ...)  \
    fprintf(stderr, "[WARNING] %s: " fmt "\n", __func__, ##__VA_ARGS__)

#ifdef DEBUG
#define dbg(fmt, ...)  \
    fprintf(stderr, "[DEBUG] %s: " fmt "\n", __func__, ##__VA_ARGS__)
#else
#define dbg(fmt, ...)
#endif /* DEBUG */

#endif /* End of MACRO_H */
