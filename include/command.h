/******************************************************************************
 * command.h: Subcommands definitions
 *****************************************************************************/
#ifndef COMMAND_H
#define COMMAND_H

enum { 
    CMD_ERR = -1,
    CMD_HELP,
    CMD_ADD,
    CMD_RM,
    CMD_MV,
    CMD_SWAP,
    CMD_EDIT,
    CMD_SHOW,
    CMD_AR,
    CMD_LSAR,
#ifdef WITH_GIT
    CMD_UNDO,
#endif
    CMD_RESTORE
};

extern char *progname;

void launch(int cmd_code, int argc, char *argv[]);
void usage(void);

#endif /* End of COMMAND_H */
