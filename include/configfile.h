/******************************************************************************
 * configfile.h: Config file related functions
 *****************************************************************************/
#ifndef CONFIGFILE_H
#define CONFIGFILE_H

#include "macro.h"

/* global settings */
#define TODOFILE_ENV	"TODOFILE"
#define HOME_ENV	"HOME"
#define CONFIG_DIR	".todo"
#define CONFIG_FILE	"config"

/* valid keys in configfile */
#define CONF_GIT_KEY	    "with_git"
#define CONF_TODOFILE_KEY   "todofile"
#define CONF_ARCHIVE_KEY    "archive"

/* configuration assertion macros */
#define assert_config() \
    do {\
	if (conf == NULL)\
	    error("BUG: config has not been set up");\
    } while (0)
#define assert_todofile() \
    do {\
	assert_config();\
	if (conf->todofile == NULL)\
	    error("BUG: todo filename has not been set");\
    } while (0)

typedef struct config {
    char *todofile;	/* full path to the todofile */
    char *archive;	/* full path to the archive file */
#ifdef WITH_GIT
    int git;		/* if set, use git for tracking changes */
#endif /* WITH_GIT */
} config_t;

extern config_t *conf;

void setup(void);
void quit(void);
char *get_configfile(void);

#endif /* End of CONFIGFILE_H */
